import gym
import numpy as np
import fh_ac_ai_gym
from itertools import product
import random
import matplotlib.pyplot as plt


class Agent:
    def __init__(self):
        self.env = gym.make('Wumpus-v0')
        self.episodes = 1000
        # parameters
        self.gamma = 0.7
        self.alpha = 0.5
        self.epsilon = 1
        self.Q = {}
        self._states()
        self.actions = [0, 1, 2, 3, 4, 5]
        # For deterministic behaviour to compare algorithms
        np.random.seed(0)
        random.seed(0)

    # All state possibilities
    def _states(self):
        self.states = []
        a = list(product([0, 1], repeat=6))
        for x in range(4):
            for y in range(4):
                for g in range(2):
                    for d in range(4):
                        for i in a:
                            self.states.append((x, y, g, d, *i))

    # initialize table with state-action-values
    def _init_Q(self):
        for s in self.states:
            for a in self.actions:
                self.Q[s, a] = 0

    # Represent state as tuple of integers
    def _get_state(self, obs):
        return obs["x"], obs["y"], int(obs["gold"]), obs["direction"].value, int(obs["arrow"]), int(obs["stench"]), \
               int(obs["breeze"]), int(obs["glitter"]), int(obs["bump"]), int(obs["scream"])

    # Epsilon-Greedy policy
    def _get_action(self, state):
        values = np.asarray([self.Q[state, a] for a in self.actions], dtype=float)
        action = np.argmax(values) if self.epsilon < np.random.random() else random.choice(self.actions)
        self.epsilon -= 1 / 1000 if self.epsilon > 0 else 0
        return action

    def sarsa(self):
        total_rewards = []
        self._init_Q()
        self.epsilon = 1
        for e in range(self.episodes):
            done = False
            obs = self.env.reset()
            s = self._get_state(obs)
            a = self._get_action(s)
            episode_reward = 0
            while not done:
                obs, reward, done, _ = self.env.step(a)
                s_ = self._get_state(obs)
                a_ = self._get_action(s_)
                self.Q[s, a] = self.Q[s, a] + self.alpha * (reward + self.gamma * self.Q[s_, a_] - self.Q[s, a])
                s, a = s_, a_
                episode_reward += reward
            print(f"Episode: {e + 1}, Reward: {episode_reward} ")
            total_rewards.append(episode_reward)
        return total_rewards

    def q_learning(self):
        total_rewards = []
        self._init_Q()
        self.epsilon = 1
        for e in range(self.episodes):
            done = False
            obs = self.env.reset()
            S = self._get_state(obs)
            episode_reward = 0
            while not done:
                A = self._get_action(S)
                obs, reward, done, _ = self.env.step(A)
                s_ = self._get_state(obs)
                q_values = np.asarray([self.Q[s_, a] for a in self.actions], dtype=float)
                self.Q[S, A] = self.Q[S, A] + self.alpha * (reward + self.gamma * np.max(q_values) - self.Q[S, A])
                S = s_
                episode_reward += reward
            print(f"Episode: {e + 1}, Reward: {episode_reward} ")
            total_rewards.append(episode_reward)
        return total_rewards

    def play(self, episodes):
        self.epsilon = 0
        for _ in range(episodes):
            done = False
            obs = self.env.reset()
            while not done:
                state = self._get_state(obs)
                action = self._get_action(state)
                obs, reward, done, _ = self.env.step(action)
                self.env.render()


if __name__ == '__main__':
    agent = Agent()
    rewards = np.asarray(agent.sarsa())

    # plot mean rewards over 50 episodes
    mean_rewards = [np.mean(rewards[i - 50:i]) for i in range(50, np.size(rewards))]
    plt.plot(mean_rewards, label='SARSA')
    rewards = np.asarray(agent.q_learning())
    mean_rewards = [np.mean(rewards[i - 50:i]) for i in range(50, np.size(rewards))]
    plt.plot(mean_rewards, label='Q-Learning')
    plt.legend()
    plt.show()
